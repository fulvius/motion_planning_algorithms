%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Rapidly Exploring Random Tree %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% init
clear all;
close all;

K = 20000;
x_min = 0;
x_max = 200;
y_min = 0;
y_max = 200;

x_init = 0;
y_init = 0;

x_goal = 200;
y_goal = 150;

threshold = 15;

tree.vertex(1).x = x_init;
tree.vertex(1).y = y_init;
tree.vertex(1).x_prev = x_init;
tree.vertex(1).y_prev = y_init;
tree.vertex(1).dist = 0;
tree.vertex(1).ind = 1;
tree.vertex(1).ind_prev = 0;

figure(1)
hold on
grid on
axis([x_min x_max y_min y_max])
plot(x_init, y_init, 'ko', 'MarkerSize', 10, 'MarkerFaceColor', 'k');
plot(x_goal, y_goal, 'go', 'MarkerSize', 10, 'MarkerFaceColor', 'g');

%% obstacle
x1 = 60;
y1 = 60;
x2 = 40;
y2 = 40;
rectangle('Position', [x1 y1 x2 y2], 'FaceColor', 'k','Curvature',[1 1])

bool = 0;
iter = 2;

%% samplimg
for iter_ = 2:1:K
    
    while bool == 0

        x_rand = (x_max - x_min) * rand;
        y_rand = (y_max - y_min) * rand;
        
        if (obstacle(x1+x2/2,y1+y2/2,x2/2,x_rand,y_rand) == -1)
            bool = 0;
        else
            bool = 1;
        end
    end
    
    bool = 0;
    
    dist = Inf * ones(1,length(tree.vertex));
    
    % compute the distance of the new sample from all the vertices
    for j = 1:length(tree.vertex)
        dist(j) = sqrt((x_rand - tree.vertex(j).x)^2 + (y_rand - tree.vertex(j).y)^2);
    end
    % take the nearest vertex to the new sample and the distance
    [val, ind] = min(dist);
        

    if val <= threshold

        tree.vertex(iter).x = x_rand;
        tree.vertex(iter).y = y_rand;
        tree.vertex(iter).dist = val;
        tree.vertex(iter).x_prev = tree.vertex(iter).x;
        tree.vertex(iter).y_prev = tree.vertex(iter).y;
        tree.vertex(iter).ind = iter;
        tree.vertex(iter).ind_prev = ind;

        if sqrt((x_rand-x_goal)^2 + (y_rand - y_goal)^2) <= threshold
            plot([tree.vertex(iter).x; tree.vertex(ind).x], [tree.vertex(iter).y; tree.vertex(ind).y], 'r')
            break
        end

        plot([tree.vertex(iter).x; tree.vertex(ind).x], [tree.vertex(iter).y; tree.vertex(ind).y], 'r')
        pause(0);
        
        iter = iter + 1;
        
    end
        
    
end

%% building path

if iter < K
    path.pos(1).x = x_goal;
    path.pos(1).y = y_goal;
    path.pos(2).x = tree.vertex(end).x;
    path.pos(2).y = tree.vertex(end).y;
    path_index = tree.vertex(end).ind_prev;
    
    j= 0;
    
    while 1
        path.pos(j+3).x = tree.vertex(path_index).x;
        path.pos(j+3).y = tree.vertex(path_index).y;
        path_index = tree.vertex(path_index).ind_prev;
        if path_index == 1
            break
        end
        j = j + 1;
    end
    
    path.pos(end+1).x = x_init;
    path.pos(end).y = y_init;
    
    
    for j = length(path.pos):-1:2
        plot([path.pos(j).x; path.pos(j-1).x], [path.pos(j).y, path.pos(j-1).y], 'b', 'Linewidth', 2);
        pause(0.1);
    end
else
    disp('No path found. Increase number of iterations and retry.');
end


%% collision detection function

function z = obstacle(x0,y0,r,x,y)

circle = (x-x0)^2 + (y-y0)^2 - r^2;

z = sign(circle);

end
    
    