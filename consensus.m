%%%%%%%%%%%%%%%%%%%%%%%%%
%% consensus protocol %%%
%%%%%%%%%%%%%%%%%%%%%%%%%

%% first run sim_consensus.slx

%% plot data
close all

figure(1)
grid on
hold on

for k=1:1:length(u1.Data(:,1))
    plot(u1.Data(k,1), u1.Data(k,2), 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r')
    plot(u2.Data(k,1), u2.Data(k,2), 'go', 'MarkerSize', 10, 'MarkerFaceColor', 'g')
    plot(u3.Data(k,1), u3.Data(k,2), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b')
    pause(0.1);
end

% plot(u1.Data(:,1), u1.Data(:,2), 'ro', 'MarkerSize', 10, 'MarkerFaceColor', 'r')
% plot(u2.Data(:,1), u2.Data(:,2), 'go', 'MarkerSize', 10, 'MarkerFaceColor', 'g')
% plot(u3.Data(:,1), u3.Data(:,2), 'bo', 'MarkerSize', 10, 'MarkerFaceColor', 'b')