%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Approximate Cell Decomposition %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% init

clear all
close all

res = 50;
xlim = 50;
ylim = 50;

X = linspace(1,xlim,res);
Y = linspace(1,ylim,res);

fig = figure(1);
hold on
grid on
axis([0 xlim 0 ylim])

% build cells
for k=1:1:res
    plot([X(k) X(k)],[1 xlim],'k')
    plot([1 ylim],[Y(k) Y(k)], 'k')
end

% obstacle
xs = 10;
xe = 10;
ys = 10;
ye = 10;
rectangle('Position',[10 10 10 10], 'FaceColor', 'k')

q_start = [5.5;5.5];
q_end = [28.5;30.5];

plot(q_start(1), q_start(2), 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k');
plot(q_end(1), q_end(2), 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k');

path.vertex(1).x = q_start(1);
path.vertex(1).y = q_start(2);

N = 100;
iter = 1;
l = 1;

% command directions
fwd_x = q_end(1) - q_start(1);
fwd_y = q_end(2) - q_start(2);
com_x = fwd_x / abs(fwd_x);
com_y = fwd_y / abs(fwd_y);

bool = 0;

%% searching the path
while iter < N
    if path.vertex(l).x ~= q_end(1)
        if obstacle(path.vertex(l).x + com_x, path.vertex(l).y) == 1
            add_x = com_x;
        else
            add_x = 0;
        end
    else
        add_x = 0;
    end
    
    path.vertex(l+1).x = path.vertex(l).x + add_x;
    
    if path.vertex(l).y ~= q_end(2)
        if obstacle(path.vertex(l+1).x, path.vertex(l).y + com_y) == 1
            add_y = com_y;
        else
            add_y = 0;
        end
    else
        add_y = 0;
    end
    
    path.vertex(l+1).y = path.vertex(l).y + add_y;
    
    l = l + 1;
    iter = iter + 1;
    
    if path.vertex(end).x == q_end(1) && path.vertex(end).y == q_end(2)
        break;
    end
end

%% plot results
for j=2:1:l
    plot([path.vertex(j).x path.vertex(j-1).x],[path.vertex(j).y path.vertex(j-1).y], 'r', 'LineWidth',1)
    pause(0.1);
end

%% collision detection function    

function bool = obstacle(x,y)
    x1 = 10;
    y1 = 10;
    
    
    x2 = 2*x1;
    y2 = 2*y1;
    
    if (x > x1 && x < x2) && (y > y1 && y<y2)
        bool = 0;
    else
        bool = 1;
    end
end



