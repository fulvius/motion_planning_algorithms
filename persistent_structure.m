%%%%%%%%%%%%%%%%%%%%%%%%%%
%% persistent structure %%
%%%%%%%%%%%%%%%%%%%%%%%%%%

%% first run sim_persistent_structure.slx

%% plot data

close all

x1 = form.Data(:,1:2);
x2 = form.Data(:,3:4);
x3 = form.Data(:,5:6);

drawArrow = @(x,y) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0 , 'Color','r', 'MaxHeadSize',0.4, 'LineWidth',0.5);

figure(1)
grid on
hold on
axis([-6 9 -6 8])

l = length(x1(:,1));

for k = 1:1:l
    plot(x1(k,1), x1(k,2),'ko', 'MarkerSize', 10, 'MarkerFaceColor', '[0.25, 0.25, 0.25]')

    plot(x2(k,1), x2(k,2),'go', 'MarkerSize', 10, 'MarkerFaceColor', '[0, 0.5, 0]')
    drawArrow([x2(k,1),x1(k,1)],[x2(k,2),x1(k,2)])
    
    plot(x3(k,1), x3(k,2),'bo', 'MarkerSize', 10, 'MarkerFaceColor', '[0, 0.4470, 0.7410]')
    drawArrow([x3(k,1),x1(k,1)],[x3(k,2),x1(k,2)])
    
    pause(0.1)
end    