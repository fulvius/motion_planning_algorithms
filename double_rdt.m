%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Double Rapidly Exploring Random Tree %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% init
clear all;
close all;

K = 10000;
x_min = 0;
x_max = 200;
y_min = 0;
y_max = 200;

x_init = 0;
y_init = 0;

x_goal = 200;
y_goal = 150;

threshold = 15;

tree1.vertex(1).x = x_init;
tree1.vertex(1).y = y_init;
tree1.vertex(1).x_prev = x_init;
tree1.vertex(1).y_prev = y_init;
tree1.vertex(1).dist = 0;
tree1.vertex(1).ind = 1;
tree1.vertex(1).ind_prev = 0;

tree2.vertex(1).x = x_goal;
tree2.vertex(1).y = y_goal;
tree2.vertex(1).x_prev = x_goal;
tree2.vertex(1).y_prev = y_goal;
tree2.vertex(1).dist = 0;
tree2.vertex(1).ind = 1;
tree2.vertex(1).ind_prev = 0;

figure(1)
hold on
grid on
axis([x_min x_max y_min y_max])
plot(x_init, y_init, 'ko', 'MarkerSize', 10, 'MarkerFaceColor', 'k');
plot(x_goal, y_goal, 'go', 'MarkerSize', 10, 'MarkerFaceColor', 'g');

%% obstacles
x1 = 60;
y1 = 60;
x2 = 40;
y2 = 40;
rectangle('Position', [x1 y1 x2 y2], 'FaceColor', 'k', 'Curvature', [1,1])

bool = 0;
iter1 = 2;
iter2 = 2;

%% sampling
for iter_ = 2:1:K
    
    
    %% first tree
    while bool == 0
        
        x_rand = (x_max - x_min) * rand;
        y_rand = (y_max - y_min) * rand;
          
        if ((x_rand > x1 && y_rand > y1) && (x_rand < (x1+x2) && y_rand < (y1+y2)))
            bool = 0;
        else
            bool = 1;
        end
    end
    bool = 0;
    
    dist = Inf * ones(1,length(tree1.vertex));
    % compute the distance of the new sample from all the vertices
    for j = 1:length(tree1.vertex)
        dist(j) = sqrt((x_rand - tree1.vertex(j).x)^2 + (y_rand - tree1.vertex(j).y)^2);
    end
    % take the nearest vertex to the new sample and the distance        
    [val, ind] = min(dist);
        
    if val < threshold

        tree1.vertex(iter1).x = x_rand;
        tree1.vertex(iter1).y = y_rand;
        tree1.vertex(iter1).dist = val;
        tree1.vertex(iter1).x_prev = tree1.vertex(iter1).x;
        tree1.vertex(iter1).y_prev = tree1.vertex(iter1).y;
        tree1.vertex(iter1).ind = iter1;
        tree1.vertex(iter1).ind_prev = ind;

        plot([tree1.vertex(iter1).x; tree1.vertex(ind).x], [tree1.vertex(iter1).y; tree1.vertex(ind).y], 'r')
        pause(0);
        
        iter1 = iter1 + 1;
        
    end
    
    %% second tree
    while bool == 0
        
        x_rand = (x_max - x_min) * rand;
        y_rand = (y_max - y_min) * rand;
          
        if ((x_rand > x1 && y_rand > y1) && (x_rand < (x1+x2) && y_rand < (y1+y2)))
            bool = 0;
        else
            bool = 1;
        end
    end
    bool = 0;
    
    dist = Inf * ones(1,length(tree2.vertex));
    
    % compute the distance of the new sample from all the vertices
    for j = 1:length(tree2.vertex)
        dist(j) = sqrt((x_rand - tree2.vertex(j).x)^2 + (y_rand - tree2.vertex(j).y)^2);
    end
    % take the nearest vertex to the new sample and the distance
    [val, ind] = min(dist);
        
    if val < threshold

        tree2.vertex(iter2).x = x_rand;
        tree2.vertex(iter2).y = y_rand;
        tree2.vertex(iter2).dist = val;
        tree2.vertex(iter2).x_prev = tree2.vertex(iter2).x;
        tree2.vertex(iter2).y_prev = tree2.vertex(iter2).y;
        tree2.vertex(iter2).ind = iter2;
        tree2.vertex(iter2).ind_prev = ind;

        plot([tree2.vertex(iter2).x; tree2.vertex(ind).x], [tree2.vertex(iter2).y; tree2.vertex(ind).y], 'g')
        pause(0);
        
        iter2 = iter2 + 1;
        
    end
    
    %% check metting
    
    DIST = Inf * ones(length(tree1.vertex),length(tree2.vertex));
    
    for t1 = 1:1:length(tree1.vertex)
        for t2 = 1:1:length(tree2.vertex)           
            DIST(t1,t2) = sqrt( (tree1.vertex(t1).x - tree2.vertex(t2).x)^2 + (tree1.vertex(t1).y - tree2.vertex(t2).y)^2 );
        end
    end
    
    val = min(min(DIST));
    [pos_1, pos_2] = find(DIST==val);
    
    if val < threshold
        plot( [tree1.vertex(pos_1).x; tree2.vertex(pos_2).x],[tree1.vertex(pos_1).y; tree2.vertex(pos_2).y], 'm')
        break
    end
    

    
    
end
    

%% buildind path   
pause(1)

T.pos(1).x = tree1.vertex(pos_1).x;
T.pos(1).y = tree1.vertex(pos_1).y;
path_index = tree1.vertex(pos_1).ind_prev;

j = 2;

while 1
    T.pos(j).x = tree1.vertex(path_index).x;
    T.pos(j).y = tree1.vertex(path_index).y;
    path_index = tree1.vertex(path_index).ind_prev;
    if path_index == 1
        break
    end
    j = j + 1;
end

T.pos(end+1).x = x_init;
T.pos(end).y = y_init;

tmp = T;
for l = 1:length(T.pos)
    T.pos(l) = tmp.pos(end-l+1);
end

E = length(T.pos);

T.pos(E+1).x = tree2.vertex(pos_2).x;
T.pos(E+1).y = tree2.vertex(pos_2).y;
path_index = tree2.vertex(pos_2).ind_prev;

j = E + 2;

while 1
    T.pos(j).x = tree2.vertex(path_index).x;
    T.pos(j).y = tree2.vertex(path_index).y;
    path_index = tree2.vertex(path_index).ind_prev;
    if path_index == 1
        break
    end
    j = j + 1;
end


T.pos(end+1).x = x_goal;
T.pos(end).y = y_goal;


for j = 2:1:length(T.pos)
    plot([T.pos(j).x; T.pos(j-1).x], [T.pos(j).y, T.pos(j-1).y], 'b', 'Linewidth', 2);
    pause(0.1);
end